POCO-Builder is a utility which allows you to quickly create POCO (Plain Old C# Objects) objects from your database entities.

---------------------------------------------------------------

Open App.Config and update your connection string

Open Models/Generated/Database.tt

Update Namespace (Match the "name" property of your Connection String)
Update RepoName (Call it whatever you want)

Save Database.tt

If you have any issues, you may need to navigate to the Database.tt file in Windows Explorer.  Right-Click on the file and select Properties.  If shown, select the button to "unblock".  Also, repeat this for the .ttinclude files in the same folder.

On successful save, a file called Database.cs will be created in the Models/Generated folder.  This file should have POCO objects for all your Database Entities.

Updated 8/31/2014, Jason Williams, smailliwnosaj@yahoo.com
Templates acquired from http://GitHub.com/PetaPoco




